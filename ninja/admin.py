from django.contrib import admin
from ninja.models import Image

class ImageAdmin(admin.ModelAdmin):
    list_display = ('filename', 'title')

admin.site.register(Image, ImageAdmin)
