__author__ = 'igor'
from django.shortcuts import render_to_response
from models import Image

def home(request):
    images  = Image.objects.all()[:7]
    return render_to_response('home.html', {'images': images,}, )
