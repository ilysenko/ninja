var cur_Image = 1;
var timer;

function right() {
    cur_Image += 1;
    if(cur_Image > images_q) cur_Image=1;
    image_switch(cur_Image);
}

function left() {
    cur_Image -= 1;
    if(cur_Image < 1) cur_Image=images_q;
    image_switch(cur_Image);
}

function image_switch(number) {
    $('.thumbnail').removeClass('active_t');
    $('#t'+number).addClass('active_t');
    $('.image').fadeOut(1000);
    $('#i'+number).fadeIn(1000);
    cur_Image = parseInt(number);
}

function scroll() {
    right();
    timer = setTimeout(function () { scroll(); }, 3000)
}

function scroll_pause() {
    clearTimeout(timer);
    timer = setTimeout(function () { scroll(); }, 10000)
}

$(document).ready(function(){
    $('.right').click(function(e) {scroll_pause(); right();e.preventDefault();});
    $('.left').click(function(e) {scroll_pause(); left();e.preventDefault();});
    $('.thumbnail').click(
            function(e) {
                if (cur_Image == $(this).attr('data-number')) return;
                scroll_pause();
                image_switch($(this).attr('data-number'));
                e.preventDefault();
            });

    image_switch(1);

    $(document).keydown(function(e){
        if(e.keyCode==37) {scroll_pause(); left();};
        if(e.keyCode==39) {scroll_pause(); right();};
    });

    timer = setTimeout(function () { scroll();}, 3000)
})