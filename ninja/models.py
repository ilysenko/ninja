__author__ = 'igor'

from django.db import models
import os

class Image(models.Model):
    title = models.CharField(max_length=255, blank=False)
    text = models.TextField(blank=False)
    image = models.ImageField(upload_to='images')

    def filename(self):
        return os.path.basename(self.image.name)
